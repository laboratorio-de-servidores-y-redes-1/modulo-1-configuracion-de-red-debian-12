# Modulo 1 - Configuración de red Debian 12



## Servicio networking

En sistemas Linux que utilizan Systemd como gestor de servicios, el servicio `networking.service` se encarga de la configuración de la red. Este servicio es esencial para establecer y gestionar las interfaces de red en el sistema.

#### Verificando el Estado del Servicio

Antes de iniciar o reiniciar el servicio, es útil verificar su estado actual. Puedes hacerlo con el siguiente comando:

```bash
sudo systemctl status networking.service

```

Este comando proporciona información sobre si el servicio está activo, inactivo o fallido.

```
● networking.service - Raise network interfaces
     Loaded: loaded (/lib/systemd/system/networking.service; enabled; preset: enabled)
     Active: active (exited) since Tue 2024-06-11 22:48:05 -03; 5min ago
       Docs: man:interfaces(5)
   Main PID: 505 (code=exited, status=0/SUCCESS)
        CPU: 32ms

jun 11 22:48:04 apache systemd[1]: Starting networking.service - Raise network interfaces...
jun 11 22:48:05 apache systemd[1]: Finished networking.service - Raise network interfaces.
```



#### Iniciando el Servicio

Para iniciar el servicio `networking.service`, utiliza el siguiente comando:

```bash
sudo systemctl start networking.service
```

Este comando inicia el servicio si no está ya en ejecución.

#### Habilitando el Servicio en el Arranque

Para asegurarte de que el servicio `networking.service` se inicie automáticamente al arrancar el sistema, debes habilitarlo:

```bash
sudo systemctl enable networking.service
```

Este comando crea los enlaces simbólicos necesarios para que Systemd inicie el servicio automáticamente en cada arranque.

#### Reiniciando el Servicio

Si realizas cambios en la configuración de red y necesitas aplicar estos cambios, puedes reiniciar el servicio con:

```bash
sudo systemctl restart networking.service
```

Este comando detiene y vuelve a iniciar el servicio, aplicando cualquier cambio en la configuración de red.

#### Deteniendo el Servicio

En algunas situaciones, puede ser necesario detener el servicio. Para hacerlo, utiliza:

```bash
sudo systemctl stop networking.service
```

Este comando detiene el servicio, lo que puede ser útil para tareas de mantenimiento o solución de problemas.

#### Deshabilitando el Servicio

Si no deseas que el servicio se inicie automáticamente al arrancar el sistema, puedes deshabilitarlo con:

```bash
sudo systemctl disable networking.service
```

Este comando elimina los enlaces simbólicos creados anteriormente, evitando que el servicio se inicie automáticamente.



## Configuración de red



### Archivo '/etc/network/interfaces'

El archivo `/etc/network/interfaces` define cómo deben configurarse las interfaces de red del sistema. Puedes especificar configuraciones de red estática o dinámica (DHCP) para cada interfaz.

#### Sintaxis del Archivo

La sintaxis básica del archivo `/etc/network/interfaces` incluye:
- **auto**: Define qué interfaces deben configurarse automáticamente al arrancar.
- **iface**: Define una interfaz y su método de configuración (e.g., DHCP o estática).

```
# cat /etc/network/interfaces
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug enp0s3
iface enp0s3 inet dhcp
```



#### The loopback network interface

La interfaz de red loopback es una característica esencial de los sistemas operativos modernos, utilizada principalmente para probar y depurar servicios de red de manera local. Representada por la dirección IP `127.0.0.1` y el nombre de interfaz `lo`, la interfaz loopback permite que un dispositivo se comunique consigo mismo, sin necesidad de una conexión física de red. 

Cuando un programa o servicio envía datos a la dirección `127.0.0.1`, esos datos son inmediatamente redirigidos de vuelta al mismo dispositivo. Esto es especialmente útil para desarrolladores y administradores de sistemas, ya que permite probar aplicaciones y servicios de red en el propio dispositivo antes de desplegarlos en una red real.

Además, la interfaz loopback asegura que ciertos servicios críticos, como servidores web locales, bases de datos y aplicaciones, puedan funcionar independientemente del estado de las interfaces de red físicas. Esto proporciona un entorno estable y predecible para el desarrollo y la prueba de software.

Las líneas `auto lo` e `iface lo inet loopback` en un archivo de configuración de red indican cómo debe gestionarse la interfaz de red loopback del sistema.

- `auto lo`: Esta línea especifica que la interfaz `lo` (loopback) debe activarse automáticamente cuando el sistema se inicia. Es decir, cada vez que el sistema arranca, se asegura de que la interfaz loopback esté operativa sin necesidad de intervención manual.

- `iface lo inet loopback`: Esta línea define la configuración de la interfaz `lo`, indicando que debe usar el método `loopback` para configurarse. Esto significa que la interfaz `lo` se usará para las comunicaciones internas del sistema, utilizando la dirección IP especial `127.0.0.1`.

En conjunto, estas líneas aseguran que la interfaz loopback esté siempre activa y configurada correctamente para permitir que el dispositivo se comunique consigo mismo de manera eficiente.

```
# The loopback network interface
auto lo
iface lo inet loopback
...
```



#### The primary network interface

La interfaz de red principal, conocida como "The primary network interface" en inglés, es la interfaz que se utiliza para la mayoría de las comunicaciones de red en un sistema. Esta interfaz conecta el dispositivo a la red local y, a través de ella, a Internet o a otras redes.

Generalmente, la interfaz de red principal tiene un nombre como `eth0`, `ens33` o similar, dependiendo de cómo el sistema operativo denomine a las interfaces de red. La configuración de esta interfaz incluye la asignación de una dirección IP, máscara de subred, puerta de enlace predeterminada y, en muchos casos, servidores DNS.

En la configuración del sistema, la interfaz de red principal se define para que se active automáticamente al iniciar el sistema y pueda gestionar el tráfico de red esencial para el funcionamiento del dispositivo. Es crucial para la comunicación con otros dispositivos en la red, el acceso a recursos compartidos y la conexión a servicios externos.

Las líneas `allow-hotplug enp0s3` e `iface enp0s3 inet dhcp` en la configuración de red indican lo siguiente:

- `allow-hotplug enp0s3`: Esta línea especifica que la interfaz `enp0s3` debe activarse automáticamente cuando se detecta que el hardware está disponible. Esto es útil para interfaces que pueden no estar presentes al inicio, como dispositivos USB o hardware de red que puede conectarse o desconectarse mientras el sistema está en funcionamiento.

- `iface enp0s3 inet dhcp`: Esta línea define que la interfaz `enp0s3` debe configurarse usando DHCP (Dynamic Host Configuration Protocol). Esto significa que la interfaz obtendrá su configuración de red (dirección IP, máscara de subred, puerta de enlace, etc.) automáticamente de un servidor DHCP en la red.

En conjunto, estas líneas configuran la interfaz `enp0s3` para que se active automáticamente cuando se conecta y para que obtenga su configuración de red de un servidor DHCP.

```
...
# The primary network interface
allow-hotplug enp0s3
iface enp0s3 inet dhcp
```



### Configuraciones

#### Ejemplo de Configuración de Red Dinámica (DHCP)

Configuración para una interfaz que utiliza DHCP para obtener su configuración de red automáticamente:

```plaintext
# Configuración de interfaz de red dinámica
auto eth0
iface eth0 inet dhcp
```

- `auto eth0`: Indica que la interfaz `eth0` debe configurarse automáticamente al iniciar.
- `iface eth0 inet dhcp`: Define que la interfaz `eth0` debe usar DHCP para obtener su configuración de red.



#### Ejemplo de Configuración de Red Estática

Configuración para una interfaz con una configuración de red estática:

```plaintext
# Configuración de interfaz de red estática
auto eth0
iface eth0 inet static
    address 192.168.1.100
    netmask 255.255.255.0
    gateway 192.168.1.1
```

- `auto eth0`: Indica que la interfaz `eth0` debe configurarse automáticamente al iniciar.
- `iface eth0 inet static`: Define que la interfaz `eth0` debe usar una configuración de red estática.
- `address 192.168.1.100`: La dirección IP asignada a la interfaz.
- `netmask 255.255.255.0`: La máscara de subred.
- `gateway 192.168.1.1`: La puerta de enlace predeterminada.

### Aplicando los Cambios en la Configuración

Después de editar el archivo `/etc/network/interfaces`, necesitas aplicar los cambios para que surtan efecto. Puedes hacerlo de la siguiente manera:

**Reiniciar el Servicio de Redes**

```bash
sudo systemctl restart networking.service
```

**Reiniciar la Interfaz de Red**

Alternativamente, puedes reiniciar la interfaz de red específica:

```bash
sudo ifdown eth0 && sudo ifup eth0
```

- `sudo ifdown eth0`: Desactiva la interfaz `eth0`.
- `sudo ifup eth0`: Activa la interfaz `eth0` con la nueva configuración.
